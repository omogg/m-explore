#include <explore/explore.h>

#include <thread>

inline static bool operator==(const geometry_msgs::Point& one,
                              const geometry_msgs::Point& two)
{
  double dx = one.x - two.x;
  double dy = one.y - two.y;
  double dist = sqrt(dx * dx + dy * dy);
  return dist < 0.01;
}

namespace explore
{
Explore::Explore()
  : private_nh_("~")
  , tf_listener_(ros::Duration(10.0))
  , costmap_client_(private_nh_, relative_nh_, &tf_listener_)
  , prev_distance_(0)
  , last_markers_count_(0)
  , plannerTimer_(20)
  , isfirstround_(true)
{
  double timeout;
  double min_frontier_size;
  private_nh_.param("planner_frequency", planner_frequency_, 1.0);
  private_nh_.param("progress_timeout", timeout, 30.0);
  progress_timeout_ = ros::Duration(timeout);
  goal_set_timeout_ = ros::Duration(5.0);
  private_nh_.param("visualize", visualize_, false);
  private_nh_.param("potential_scale", potential_scale_, 1e-3);
  private_nh_.param("orientation_scale", orientation_scale_, 0.0);
  private_nh_.param("gain_scale", gain_scale_, 1.0);
  private_nh_.param("min_frontier_size", min_frontier_size, 0.5);
  private_nh_.param("used_planner",used_planner_,std::string("smb_path_planner"));

  search_ = frontier_exploration::FrontierSearch(costmap_client_.getCostmap(),
                                                 potential_scale_, gain_scale_,
                                                 min_frontier_size);

  if (visualize_) {
    marker_array_publisher_ =
        private_nh_.advertise<visualization_msgs::MarkerArray>("frontiers", 10);
  }

  if (used_planner_ == "smb_path_planner"){
	  //status 0: robot is running
	  //status 1: robot reached goal
	  //status 2: goal unreachable
	  robot_status_ = 0;
	  subs_rosout_agg_ = private_nh_.subscribe("/rosout_agg",1,&Explore::callback_rosout_agg, this);
	  makePlanSMB();
	  exploring_timer_ =
	      relative_nh_.createTimer(ros::Duration(1. / planner_frequency_),
	                               [this](const ros::TimerEvent&) { makePlanSMB(); });
  }
}

Explore::~Explore()
{
  stop();
}

void Explore::visualizeFrontiers(
    const std::vector<frontier_exploration::Frontier>& frontiers)
{
  std_msgs::ColorRGBA blue;
  blue.r = 0;
  blue.g = 0;
  blue.b = 1.0;
  blue.a = 1.0;
  std_msgs::ColorRGBA red;
  red.r = 1.0;
  red.g = 0;
  red.b = 0;
  red.a = 1.0;
  std_msgs::ColorRGBA green;
  green.r = 0;
  green.g = 1.0;
  green.b = 0;
  green.a = 1.0;

  ROS_DEBUG("visualising %lu frontiers", frontiers.size());
  visualization_msgs::MarkerArray markers_msg;
  std::vector<visualization_msgs::Marker>& markers = markers_msg.markers;
  visualization_msgs::Marker m;

  m.header.frame_id = costmap_client_.getGlobalFrameID();
  m.header.stamp = ros::Time::now();
  m.ns = "frontiers";
  m.scale.x = 1.0;
  m.scale.y = 1.0;
  m.scale.z = 1.0;
  m.color.r = 0;
  m.color.g = 0;
  m.color.b = 255;
  m.color.a = 255;
  // lives forever
  m.lifetime = ros::Duration(0);
  m.frame_locked = true;

  // weighted frontiers are always sorted
  double min_cost = frontiers.empty() ? 0. : frontiers.front().cost;

  m.action = visualization_msgs::Marker::ADD;
  size_t id = 0;
  for (auto& frontier : frontiers) {
    m.type = visualization_msgs::Marker::POINTS;
    m.id = int(id);
    m.pose.position = {};
    m.scale.x = 0.1;
    m.scale.y = 0.1;
    m.scale.z = 0.1;
    m.points = frontier.points;
    if (goalOnBlacklist(frontier.centroid)) {
      m.color = red;
    } else {
      m.color = blue;
    }
    markers.push_back(m);
    ++id;
    m.type = visualization_msgs::Marker::SPHERE;
    m.id = int(id);
    m.pose.position = frontier.initial;
    // scale frontier according to its cost (costier frontiers will be smaller)
    double scale = std::min(std::abs(min_cost * 0.4 / frontier.cost), 0.5);
    m.scale.x = scale;
    m.scale.y = scale;
    m.scale.z = scale;
    m.points = {};
    m.color = green;
    markers.push_back(m);
    ++id;
  }
  size_t current_markers_count = markers.size();

  // delete previous markers, which are now unused
  m.action = visualization_msgs::Marker::DELETE;
  for (; id < last_markers_count_; ++id) {
    m.id = int(id);
    markers.push_back(m);
  }

  last_markers_count_ = current_markers_count;
  marker_array_publisher_.publish(markers_msg);
}

void Explore::makePlanSMB()
{
  // find frontiers
  auto pose = costmap_client_.getRobotPose();
  // get frontiers sorted according to cost
  auto frontiers = search_.searchFrom(pose.position);
  ROS_DEBUG("found %lu frontiers", frontiers.size());
  for (size_t i = 0; i < frontiers.size(); ++i) {
    ROS_DEBUG("frontier %zd cost: %f", i, frontiers[i].cost);
  }

  if (frontiers.empty()) {
    stop();
    return;
  }

  // publish frontiers as visualization markers
  if (visualize_) {
    visualizeFrontiers(frontiers);
  }

  if (isfirstround_){
	  current_goal_.x = pose.position.x;
	  current_goal_.y = pose.position.y;
	  isfirstround_ = false;
  }

  // find non blacklisted frontier
  auto frontier =
      std::find_if_not(frontiers.begin(), frontiers.end(),
                       [this](const frontier_exploration::Frontier& f) {
                         return goalOnBlacklist(f.centroid);
                       });
  if (frontier == frontiers.end()) {
    stop();
    return;
  }
  geometry_msgs::Point target_position = frontier->centroid;

  // time out if we are not making any progress
  bool same_goal = prev_goal_ == target_position;
  geometry_msgs::Point old_goal = prev_goal_;
  prev_goal_ = target_position;
  if (!same_goal || prev_distance_ > frontier->min_distance) {
    // we have different goal or we made some progress
    last_progress_ = ros::Time::now();
    prev_distance_ = frontier->min_distance;
  }
  // black list if we've made no progress for a long time
  if (ros::Time::now() - last_progress_ > progress_timeout_) {
    frontier_blacklist_.push_back(target_position);
    ROS_DEBUG("Adding current goal to black list");
    makePlanSMB();
    return;
  }

  // we don't need to do anything if we still pursuing the same goal

  //std::cout << "SAME GOAL:" << same_goal << "; ROBOT STATUS" << robot_status_ << std::endl;
  if (same_goal && robot_status_ == 0) {
    return;
  }

  geometry_msgs::PoseStamped goal;
  goal.header.frame_id = costmap_client_.getGlobalFrameID();
  goal.header.stamp = ros::Time::now();
  goal.pose.position.x = target_position.x;
  goal.pose.position.y = target_position.y;
  goal.pose.position.z = target_position.z;
  goal.pose.orientation.w = 1.;

  geometry_msgs::PoseStamped start;
  start.header.frame_id = costmap_client_.getGlobalFrameID();
  start.header.stamp = ros::Time::now();
  start.pose.position.x = pose.position.x;
  start.pose.position.y = pose.position.y;
  start.pose.position.z = pose.position.z;
  start.pose.orientation.w = pose.orientation.w;
  start.pose.orientation.x = pose.orientation.x;
  start.pose.orientation.y = pose.orientation.y;
  start.pose.orientation.z = pose.orientation.z;


  callGlobalPlannerService(start, goal);

  current_goal_.x = target_position.x;
  current_goal_.y = target_position.y;

  robot_status_ = 0;
  last_time_goal_set_ = ros::Time::now();

}

bool Explore::goalOnBlacklist(const geometry_msgs::Point& goal)
{
  constexpr static size_t tolerace = 5;
  costmap_2d::Costmap2D* costmap2d = costmap_client_.getCostmap();

  // check if a goal is on the blacklist for goals that we're pursuing
  for (auto& frontier_goal : frontier_blacklist_) {
    double x_diff = fabs(goal.x - frontier_goal.x);
    double y_diff = fabs(goal.y - frontier_goal.y);

    if (x_diff < tolerace * costmap2d->getResolution() &&
        y_diff < tolerace * costmap2d->getResolution())
      return true;
  }
  return false;
}


void Explore::callGlobalPlannerService(geometry_msgs::PoseStamped start, geometry_msgs::PoseStamped goal) {

  std::string service_name = "compute_global_path";
  geometry_msgs::PoseStamped start_pose, goal_pose;

  start_pose = start;
  goal_pose = goal;

  std::thread t([service_name, start_pose, goal_pose] {
    smb_planner_msgs::PlannerService req;
    req.request.goal_pose = goal_pose;

    try {
      ROS_DEBUG_STREAM("Service name: " << service_name);
      if (!ros::service::call(service_name, req)) {
        ROS_WARN_STREAM("Couldn't call service: " << service_name);
      }
    } catch (const std::exception &e) {
      ROS_ERROR_STREAM("Service Exception: " << e.what());
    }
  });
  t.detach();
}

void Explore::start()
{
  exploring_timer_.start();
}

void Explore::stop()
{
  exploring_timer_.stop();
  ROS_INFO("Exploration stopped.");
}

void Explore::callback_rosout_agg(const rosgraph_msgs::Log::ConstPtr& msg){
	if (msg->msg == "[Smb Local Planner] Global goal reached!"){
		robot_status_ = 1;
		//std::cout << "STATUS: GOAL REACHED:" << robot_status_ << std::endl;
		if (ros::Time::now() - last_time_goal_set_ > goal_set_timeout_){
			makePlanSMB();
		}
	}
	if (msg->msg == "[Smb Local Planner][CHOMP Solver] No covergence!"){
		robot_status_ = 2;
		//std::cout << "STATUS: CHOMP SOLVER NO CONVERGENCE:" << robot_status_ << std::endl;
		if (ros::Time::now() - last_time_goal_set_ > goal_set_timeout_){
			makePlanSMB();
		}
	}
	if (msg->msg == "[Smb Global Planner] OMPL could not find an exact solution."){
		robot_status_ = 2;
		//std::cout << "STATUS: OMPL COULD NOT FIND AN EXACT SOLUTION:" << robot_status_ << std::endl;
		if (ros::Time::now() - last_time_goal_set_ > goal_set_timeout_){
			makePlanSMB();
		}
	}
	auto pose = costmap_client_.getRobotPose();
	if (abs(current_goal_.x-pose.position.x) < 1.0 && abs(current_goal_.y-pose.position.y) < 1.0){
		//std::cout << "tol reached!" << std::endl;
		if (ros::Time::now() - last_time_goal_set_ > goal_set_timeout_){
			makePlanSMB();
		}
	}

}

}  // namespace explore


int main(int argc, char** argv)
{
  ros::init(argc, argv, "explore");
  if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,
                                     ros::console::levels::Debug)) {
    ros::console::notifyLoggerLevelsChanged();
  }
  explore::Explore explore;
  ros::spin();

  return 0;
}
