#ifndef NAV_EXPLORE_H_
#define NAV_EXPLORE_H_

#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include <actionlib/client/simple_action_client.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>

#include <explore/costmap_client.h>
#include <explore/frontier_search.h>

#include <smb_planner_msgs/PlannerService.h>

#include <rosgraph_msgs/Log.h>

namespace explore
{
/**
 * @class Explore
 * @brief A class adhering to the robot_actions::Action interface that moves the
 * robot base to explore its environment.
 */
class Explore
{
public:
  Explore();
  ~Explore();

  void start();
  void stop();

private:
  /**
   * @brief  Make a global plan
   */
  void makePlanSMB();

  /**
   * @brief  Publish a frontiers as markers
   */
  void visualizeFrontiers(
      const std::vector<frontier_exploration::Frontier>& frontiers);

  void callGlobalPlannerService(geometry_msgs::PoseStamped start, geometry_msgs::PoseStamped goal);

  void callback_rosout_agg(const rosgraph_msgs::Log::ConstPtr& msg);

  bool goalOnBlacklist(const geometry_msgs::Point& goal);

  ros::NodeHandle private_nh_;
  ros::NodeHandle relative_nh_;
  ros::Subscriber subs_rosout_agg_;
  ros::Publisher marker_array_publisher_;
  tf::TransformListener tf_listener_;
  std::mutex mutex_robot_status_;

  Costmap2DClient costmap_client_;
  frontier_exploration::FrontierSearch search_;
  ros::Timer exploring_timer_;
  ros::Timer oneshot_;

  std::vector<geometry_msgs::Point> frontier_blacklist_;
  geometry_msgs::Point prev_goal_;
  double prev_distance_;
  ros::Time last_progress_;
  ros::Time last_time_goal_set_;
  size_t last_markers_count_;
  int robot_status_;

  //ros::Time countdown_;
  int plannerTimer_;
  geometry_msgs::Point current_goal_;
  bool isfirstround_;

  // parameters
  double planner_frequency_;
  double potential_scale_, orientation_scale_, gain_scale_;
  ros::Duration progress_timeout_;
  ros::Duration goal_set_timeout_;
  bool visualize_;
  std::string used_planner_;
};
}

#endif
