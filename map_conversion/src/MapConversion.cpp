#include "map_conversion/MapConversion.hpp"

namespace map_conversion {

MapConversion::MapConversion(ros::NodeHandle& nodeHandle) :
  nodeHandle_(nodeHandle)
{
	MapConversion::get_param();

	MapConversion::init();

}

MapConversion::~MapConversion()
{
}

void MapConversion::get_param(){

  nodeHandle_.getParam("map/used_package",input_map_);
  nodeHandle_.getParam("map/topic_occupancyGrid",topic_traversability_occupancyGrid_);
  nodeHandle_.getParam("map/queue_occupancyGrid",queue_traversability_occupancyGrid_);

  nodeHandle_.getParam("traversability/topic_gridMap",topic_traversability_gridMap_);
  nodeHandle_.getParam("traversability/queue_gridMap",queue_traversability_gridMap_);
  nodeHandle_.getParam("traversability/trinary",occupancyGrid_trinary_);
  nodeHandle_.getParam("traversability/threshold_trinary",p_threshold_);

  nodeHandle_.getParam("voxblox/topic_voxblox",topic_voxblox_);
  nodeHandle_.getParam("voxblox/queue_voxblox",queue_voxblox_);
  nodeHandle_.getParam("voxblox/map_resolution_voxblox",map_resolution_);
}

void MapConversion::init(){

	if (input_map_ == "elevation_mapping"){
		subscriber_traversability_map_ = nodeHandle_.subscribe(topic_traversability_gridMap_,queue_traversability_gridMap_,&MapConversion::Callback_traversability_conversion,this);
		publisher_traversability_map_ = nodeHandle_.advertise<nav_msgs::OccupancyGrid>(topic_traversability_occupancyGrid_,queue_traversability_occupancyGrid_);
	}
	else if (input_map_ == "voxblox"){
		sub_esdf_slice_ = nodeHandle_.subscribe(topic_voxblox_, queue_voxblox_, &MapConversion::convertVoxbloxToOccupancyGrid, this);
		publisher_traversability_map_ = nodeHandle_.advertise<nav_msgs::OccupancyGrid>(topic_traversability_occupancyGrid_,queue_traversability_occupancyGrid_);
	}


	}

void MapConversion::convertVoxbloxToOccupancyGrid(const sensor_msgs::PointCloud2::ConstPtr& msg){

	sensor_msgs::PointCloud2 es1 = *msg;
	pcl::PCLPointCloud2 es2;
	pcl_conversions::toPCL(es1,es2);
	pcl::PointCloud<pcl::PointXYZI> esdf_slice;
	pcl::fromPCLPointCloud2(es2,esdf_slice);

	x_max_ = esdf_slice.points[0].x;
	x_min_ = esdf_slice.points[0].x;
	y_max_ = esdf_slice.points[0].y;
	y_min_ = esdf_slice.points[0].y;

	pcl::PointCloud<pcl::PointXYZI>::iterator it;
	for (it = esdf_slice.points.begin(); it < esdf_slice.points.end(); it++){

	      if(it->x > x_max_){
	    	  x_max_=it->x;
	      }
	      if(it->x < x_min_){
	    	  x_min_=it->x;
	      }
	      if(it->y > y_max_){
	    	  y_max_=it->y;
	      }
	      if(it->y < y_min_){
	    	  y_min_=it->y;
	      }
	}

	center_x_ = x_min_;
	center_y_ = y_min_;

	map_width_ = (x_max_-x_min_+2*map_resolution_)/map_resolution_ + 0.5;
	map_height_ = (y_max_-y_min_+2*map_resolution_)/map_resolution_ + 0.5;

	nav_msgs::OccupancyGrid occupancyGrid;
	occupancyGrid.header.frame_id = esdf_slice.header.frame_id;
	//occupancyGrid.header.stamp = esdf_slice.header.stamp;
	occupancyGrid.info.map_load_time = occupancyGrid.header.stamp;  // Same as header stamp as we do not load the map.
	occupancyGrid.info.resolution = map_resolution_;
	occupancyGrid.info.width = map_width_;
	occupancyGrid.info.height = map_height_;
	occupancyGrid.info.origin.position.x = center_x_;
	occupancyGrid.info.origin.position.y = center_y_;
	occupancyGrid.info.origin.position.z = 0.0;
	occupancyGrid.info.origin.orientation.x = 0.0;
	occupancyGrid.info.origin.orientation.y = 0.0;
	occupancyGrid.info.origin.orientation.z = 0.0;
	occupancyGrid.info.origin.orientation.w = 1.0;
	size_t nCells = (int)map_width_*(int)map_height_;
	occupancyGrid.data.resize(nCells);

	float value = -1;

	  for (std::vector<int8_t>::iterator iterator = occupancyGrid.data.begin();
	      iterator != occupancyGrid.data.end(); ++iterator) {

		  size_t itest = std::distance(occupancyGrid.data.begin(), iterator);

		  occupancyGrid.data[itest] = value;
	  }

	  size_t ind;

	  for (it = esdf_slice.points.begin(); it < esdf_slice.points.end(); it++){

		  ind = MapConversion::XYtoGridIndex(it->x,it->y);

		  if(ind >= 0 && ind < nCells){
		  if (it->intensity >= 0.0){
			  occupancyGrid.data[ind] = 0;
		  }
		  else {
			  occupancyGrid.data[ind] = 100;
		  }
		  }
	  }

	publisher_traversability_map_.publish(occupancyGrid);

}


size_t MapConversion::XYtoGridIndex(float x, float y){
	size_t index;

	index = (int)((y-center_y_)/map_resolution_+0.5)*(int)map_width_ + ((int)((x-center_x_)/map_resolution_ + 0.5)-(int)map_width_);

	return index;
}

/*
 * Below is a first attempt to build an occupancy grid map using multiple esdf layers. STILL UNFINISHED.
 */
/*
void MapConversion::convertVoxbloxToOccupancyGrid(const voxblox_msgs::Layer& msg){
	//safe msg
	voxblox::EsdfMap::Ptr esdf_map;

	bool success = voxblox::deserializeMsgToLayer(msg,esdf_map->getEsdfLayerPtr());

	//voxblox::deserializeMsgToLayer<EsdfVoxel>(msg, esdf_map->getEsdfLayerPtr());

	if (!success) {
	  ROS_ERROR_THROTTLE(10, "Got an invalid ESDF map message!");
	} else {
	  ROS_INFO_ONCE("Got an ESDF map from ROS topic!");
	}

	voxblox::BlockIndexList blocks;
	esdf_map->getEsdfLayerPtr()->getAllAllocatedBlocks(&blocks);

	  // Cache layer settings.
	  size_t vps = esdf_map->getEsdfLayerPtr()->voxels_per_side();
	  size_t num_voxels_per_block = vps * vps * vps;

	  // Temp variables.
	  voxblox::Color color;

	  // Iterate over all blocks.
	  for (const voxblox::BlockIndex& index : blocks) {
	    // Iterate over all voxels in said blocks.
	    const voxblox::Block& block = esdf_map->getEsdfLayerPtr()->getBlockByIndex(index);

		  double x_max = block.computeCoordinatesFromLinearIndex(0).x();
		  double x_min = block.computeCoordinatesFromLinearIndex(0).x();
		  double y_max = block.computeCoordinatesFromLinearIndex(0).y();
		  double y_min = block.computeCoordinatesFromLinearIndex(0).y();

	    for (size_t linear_index = 0; linear_index < num_voxels_per_block;
	         ++linear_index) {
	      voxblox::Point coord = block.computeCoordinatesFromLinearIndex(linear_index);

	      if(coord.x()>x_max){
	    	  x_max=coord.x();
	      }
	      if(coord.x()<x_min){
	    	  x_min=coord.x();
	      }
	      if(coord.y()>y_max){
	    	  y_max=coord.y();
	      }
	      if(coord.y()<y_min){
	    	  y_min=coord.y();
	      }
	      /*
	      if (vis_function(block.getVoxelByLinearIndex(linear_index), coord,
	                       &color)) {
	        pcl::PointXYZRGB point;
	        point.x = coord.x();
	        point.y = coord.y();
	        point.z = coord.z();
	        point.r = color.r;
	        point.g = color.g;
	        point.b = color.b;
	      }
	      */
/*
	    }
	  }

	//build occupancy grid
	nav_msgs::OccupancyGrid occupancyGrid;
	occupancyGrid.header.frame_id = std::string("world");
	//occupancyGrid.header.stamp.fromNSec(gridMap.getTimestamp());
	occupancyGrid.info.map_load_time = occupancyGrid.header.stamp;  // Same as header stamp as we do not load the map.
	occupancyGrid.info.resolution = esdf_map->getEsdfLayerPtr()->voxel_size_;
	occupancyGrid.info.width = esdf_map->;
	occupancyGrid.info.height = gridMap.getSize()(1);
	occupancyGrid.info.origin.position.x = 0.0;
	occupancyGrid.info.origin.position.y = 0.0;
	occupancyGrid.info.origin.position.z = 0.0;
	occupancyGrid.info.origin.orientation.x = 0.0;
	occupancyGrid.info.origin.orientation.y = 0.0;
	occupancyGrid.info.origin.orientation.z = 0.0;
	occupancyGrid.info.origin.orientation.w = 1.0;
	size_t nCells = gridMap.getSize().prod();
	occupancyGrid.data.resize(nCells);

	  // Occupancy probabilities are in the range [0,100]. Unknown is -1.
	  const float cellMin = 0;
	  const float cellMax = 100;
	  const float cellRange = cellMax - cellMin;

	  for (GridMapIterator iterator(gridMap); !iterator.isPastEnd(); ++iterator) {
	    float value = (gridMap.at(layer, *iterator) - dataMin) / (dataMax - dataMin);
	    if (isnan(value))
	      value = -1;
	    else
	      value = cellMin + min(max(0.0f, value), 1.0f) * cellRange;
	    size_t index = getLinearIndexFromIndex(iterator.getUnwrappedIndex(), gridMap.getSize(), false);
	    // Reverse cell order because of different conventions between occupancy grid and grid map.
	    occupancyGrid.data[nCells - index - 1] = value;
	  }
	//iterate through esdf

	// add to occupancy grid

	//publish occupancy grid
}
*/

void MapConversion::Callback_traversability_conversion(const grid_map_msgs::GridMap::ConstPtr& msg){
  grid_map::GridMap a;
  grid_map::GridMapRosConverter::fromMessage(*msg,a);

  for (grid_map::GridMapIterator iterator(a); !iterator.isPastEnd(); ++iterator) {
    if (std::isnan(a.at("traversability", *iterator)) == false){
    a.at("traversability", *iterator) = 1.0-a.at("traversability", *iterator);
    //in case of trinary distinction (occupied / free / unknown)
    if (occupancyGrid_trinary_)
    {
      if (a.at("traversability", *iterator) >= p_threshold_){
        a.at("traversability", *iterator) = 1.0;
      }
      else{
        a.at("traversability", *iterator) = 0.0;
      }
    }
    }
  }

  nav_msgs::OccupancyGrid occupancyGridResult;
  grid_map::GridMapRosConverter::toOccupancyGrid(a, "traversability", 0.0, 1.0, occupancyGridResult);
  publisher_traversability_map_.publish(occupancyGridResult);
}

} /* namespace */
