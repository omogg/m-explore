#include <ros/ros.h>
#include "map_conversion/MapConversion.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_conversion");
  ros::NodeHandle nodeHandle("~");

  map_conversion::MapConversion mapConversion(nodeHandle);

  ros::spin();
  return 0;
}
