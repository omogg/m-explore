#pragma once

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <grid_map_msgs/GridMap.h>
#include <grid_map_ros/grid_map_ros.hpp>
#include <grid_map_ros/GridMapRosConverter.hpp>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/io.h>
#include <voxblox_msgs/Layer.h>
#include <voxblox_ros/esdf_server.h>

namespace map_conversion {

/*!
 * Class containing the map conversion
 */
class MapConversion {
public:
	/*!
	 * Constructor.
	 */
	MapConversion(ros::NodeHandle& nodeHandle);

	/*!
	 * Destructor.
	 */
	virtual ~MapConversion();


private:
	ros::NodeHandle nodeHandle_;

	ros::Subscriber subscriber_traversability_map_;

	ros::Subscriber sub_esdf_slice_;

	ros::Publisher publisher_traversability_map_;

	std::string input_map_;


  std::string topic_traversability_gridMap_;
  std::string topic_traversability_occupancyGrid_;
  std::string topic_voxblox_;

  int queue_traversability_gridMap_;
  int queue_traversability_occupancyGrid_;
  int queue_voxblox_;

	bool occupancyGrid_trinary_;
	double p_threshold_;

	double x_max_;
	double x_min_;
	double y_max_;
	double y_min_;
	double map_width_;
	double map_height_;
	double center_x_;
	double center_y_;
	double map_resolution_;

	void get_param();

	void init();

	void Callback_traversability_conversion(const grid_map_msgs::GridMap::ConstPtr& msg);

	void convertVoxbloxToOccupancyGrid(const sensor_msgs::PointCloud2::ConstPtr& msg);

	size_t XYtoGridIndex(float x, float y);

	//void convertVoxbloxToOccupancyGrid(const voxblox_msgs::Layer& msg);

};

} /* namespace */

