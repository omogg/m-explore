# m-explore

## Overview

This package can be used for frontier-based exploration. This node allows a robot to explore its environment until no frontiers are available. It is based on [explore_lite](https://github.com/hrnr/m-explore) and is meant for use with Super Mega Bot (SMB).

Features:

* **Supported mapping libraries:** [voxblox](https://github.com/ethz-asl/voxblox), [elevation_mapping](https://github.com/ANYbotics/elevation_mapping) with [traversability_estimation](https://github.com/leggedrobotics/traversability_estimation) and [costmap_2d](http://wiki.ros.org/costmap_2d).  
* **Supported planners:** [smb_path_planner](https://github.com/VIS4ROB-lab/smb_path_planner) and [move_base](https://github.com/ros-planning/navigation).

The package has been tested under Ubuntu 18.04.

## Installation

### Case 1 - use with `smb_path_planner`:

* Set up `smb_path_planner` as instructed in [smb_path_planner](https://github.com/ros-planning/navigation).  
* catkin build explore_smb and map_conversion.  

### Case 2 - Use with `move_base`:

* Set up `smb_mt_navigation` as instructed in [smb_mt](https://bitbucket.org/omogg/smb_mt/src/summer_school/).  
* Build [explore_lite](https://github.com/hrnr/m-explore) and use launch files from `smb_mt_explore` (located in explore_smb/launch/explore_lite) with `explore_lite`.  

## Packages

There are two packages:

* `explore_smb`: Contains the frontier exploration algorithm, uses map type nav_msgs/occupancyGrid as input.  
* `map_conversion`: Converts a grid_map traversability_estimation or esdf_slices from Voxblox into nav_msgs/occupancyGrid.  

## How to run frontier exploration


### Case 1 - use with `smb_path_planner`:  


First, run the simulation:  
```asm
$ roslaunch smb_sim sim_path_planner.launch
```
Select `SmbPathFollowingController` in the controller panel and press play (use refresh if it does not show up).

(Optional) Start `elevation_mapping` node:
```asm
$ roslaunch smb_local_planner smb_elevation_mapping_simulation.launch
``` 
Note: `elevation_mapping` is only used if the parameter `check_traversability` in smb_planner_common/cfg/smb_planner_parameters_simulation.yaml is set to true.

Start the planner:
```asm
$ roslaunch smb_local_planner smb_planner_simulation.launch
```
Run map_conversion node (make sure parameter used_package is set to voxblox):
```asm
$ roslaunch map_conversion map_conversion.launch
```
Launch explore_smb:
```asm
$ roslaunch explore_smb explore_smb.launch
```

Finally, press the button `Start Local Planner`.  



### Case 2 - use `smb_mt`:  


Before use it is required to uncomment to sections the code as described in [smb_mt](https://bitbucket.org/omogg/smb_mt/src/summer_school/) readme.  


Then, run the simulation and planner (multiple planner versions are available):
```asm
$ roslaunch smb_mt_highlevel_controller smb_full.launch
```
```asm
$ roslaunch smb_mt_navigation smb_move_base_navigation_2d.launch
```
For visualization:
```asm
$ roslaunch smb_mt_viz view_robot.launch
```
Run explore_lite (with explore.launch from explore_smb):
```asm
$ roslaunch explore_lite explore.launch
```

![alt_text](explore_smb/doc/frontier_exploration1.png =400X)

Visualization of simulation with frontiers (blue dotted lines).

![alt_text](explore_smb/doc/frontier_exploration2.png =400X)

esdf_slice that frontiers are based on.

# Issues

Note that this package as well as the smb_path_planner are still a work in progress. Which is why various issues are still present. 

* Frontier exploration has to send goals infrequently since smb_path_planner takes a long time to calculate new global path (and stops any movement while doing that).
* Communication between smb_path_planner and explore_smb still needs adjustments.








